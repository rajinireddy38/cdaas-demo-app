package com.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public class DemoController {
	public static String artifactory() {
		return "https://172.31.45.65/";
	}
	public static String pgadmin() {
		return "https://172.31.45.127/pgadmin4";
	}
	public static String adminer() {
		return "https://172.31.45.127/adminer";
	}

    public static String gitlab() {
		return "https://172.31.45.65/gitlab";
	}
}
